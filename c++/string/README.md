# 基本介绍
本目录下是一个用c++实现的一个能动态分配大小的string
- 之后大家在实现一个类时，用到的大部分知识点也都基本涵盖到了，以后可以模仿着写（尤其是某些比如重载运算符的函数）
- 大家可以在**边学习[网课](https://www.bilibili.com/video/BV1ob411q7vb?p=1)边看这个代码**，逐步看懂这里代码的意思
- 本次内容的**学习目标**是
	- 以后可以模仿着`MyString`的代码实现一个类（必须）
	- 通过本次的代码，更加深入地理解引用、const、堆栈等c++知识点（不必须）

# 文件结构解释
- PS：一个 main 函数就能生成一个可执行文件
```shell
.
├── CMakeLists.txt       // cmake 必要的文件，没他不能 cmake
├── include
│   └── my_string.h      // !!! MyString 类的头文件 !!!
├── main     // 下面的几个.cpp文件，每个都有一个 main 函数，每个都能生成一个可执行文件
│   ├── main.cpp              // 生成可执行文件 main
│   ├── q1.cpp                // 生成可执行文件 q1
│   ├── q2.cpp                // 生成可执行文件 q2
│   ├── q3.cpp                // 生成可执行文件 q3
│   ├── q4.cpp                // 生成可执行文件 q4
│   └── q5.cpp                // 生成可执行文件 q5
└── src
    └── my_string.cpp    // !!! MyString 类的实现 !!!
```
- 这些 main 函数内容如下：
	- `main.cpp` 里有四个函数，测试 `MyString` 的功能实现情况
	- 其余的 `qx.cpp` 每个文件都是一个思考题，学有余力的同学可以看看想想

# 如何运行
大家可能也用过两三次 `cmake` 了，其实基本上就这个套路，都是这几行命令
```shell
# 新建一个文件夹 build
mkdir build

# 工作路径进入 build
cd build

# cmake 构建项目
cmake ..

# make 编译
make -j6

# 运行
./<可执行文件名>  # 比如：./main 就运行可执行文件 main，这个项目生成的6个可执行文件名上面都已经说明了
```
