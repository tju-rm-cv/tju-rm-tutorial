#include "my_string.h"


using namespace std;


/* MyString 功能测试 */

// 拷贝构造函数测试
void test_copy_constructor() {
    MyString str1("hello");
    MyString str2(str1);
    cout << "str1: " << str1 << endl;
    cout << "str2: " << str2 << endl;
}

// 动态大小expand, size, max_size, 运算符 +=, << 测试
void test_new() {
    MyString s;
    for (int i = 0; i < 100; i++) {
        s += "a";
        cout << "[" << s.size() << "/"<< s.max_size() << "]: " << s << endl;
    }
}

// 运算符 +, =, +=, <<, == 测试
void test_operator() {
    MyString s1("hello");
    MyString s2("world");
    MyString s3 = s1 + " " + s2;    // hello world
    cout << s3 << endl;
    s3 += "!";                      // hello world!
    cout << s3 << endl;
    s3 = s3 + "!";                  // hello world!!
    cout << s3 << endl;
    s3 = "!" + s3;                  // !hello world!!
    cout << s3 << endl;

    MyString s4("hello");
    cout << (s1 == s4) << endl;
    cout << (s1 == s2) << endl;
}

// 运算符 [] 测试
void test_index() {
    MyString s("hello");
    for (int i = 0; i < s.size(); i++) {
        cout << s[i] << " ";
    }
    cout << endl;
    for (int i = 0; i < s.size(); i++) {
        s[i] = 'a';
    }
    cout << s << endl;
}


int main() {
    /* 检查 MyString 所有功能 */
    cout << "-------------------" << "test_copy_constructor" << "-------------------" << endl;
    test_copy_constructor();
    cout << "-------------------" << "test_new" << "-------------------" << endl;
    test_new();
    cout << "-------------------" << "test_operator" << "-------------------" << endl;
    test_operator();
    cout << "-------------------" << "test_index" << "-------------------" << endl;
    test_index();

    return 0;
}
