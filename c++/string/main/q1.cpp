#include "my_string.h"


using namespace std;


int main() {
    /**
     * 1. 如果不写拷贝构造函数，会有什么问题？
     *
     *      如果不写拷贝构造函数，那么编译器会自动生成一个默认的构造函数
     *      但是这个默认的构造函数只是简单的将成员变量进行初始化，而没有进行深拷贝
     *
     *      TODO: 把拷贝构造函数注释掉，然后运行下面的函数，看看我们写的构造函数和默认构造函数有什么不同
     */
    MyString s1("Hello");
    MyString s2(s1);
    s2[0] = 'a';
    cout << s1 << endl;
    cout << s2 << endl;

    return 0;
}
