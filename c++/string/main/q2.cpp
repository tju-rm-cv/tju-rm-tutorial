#include "my_string.h"


using namespace std;


int main() {
    /**
     * 2. 如果 + 写成成员函数，会有什么问题？
     *      这部分网课中讲了，这里就不再赘述了
     *
     *      TODO: 把 operator+ 改成成员函数(把现在的 operator+ 改成被注释掉的函数)，然后看看下面哪句话会报错
     */
    MyString s1("world");
    MyString s2 = s1 + "!";
    MyString s3 = "hello " + s1;
    cout << s2 << endl;
    cout << s3 << endl;

    return 0;
}
