#include "my_string.h"


using namespace std;


int main() {
    /**
     * 3. = 函数 为什么要有返回值？
     *      提示： = 是右结合律，也就是说 a = b = c 等价于 a = (b = c)
     *    (所以有时 把 if (a == b) 写成 if (a = b) 的 bug 都不会报错)
     */
    MyString s1("hello"), s2, s3;
    s3 = s2 = s1;
    cout << s2 << endl;
    cout << s3 << endl;

    return 0;
}
