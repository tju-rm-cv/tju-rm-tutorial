#include "my_string.h"


using namespace std;


int main() {
    /**
     * 4. 为什么要有 const 修饰 [] 运算符，没这个带 const 的 [] 运算符会有什么问题？
     *
     *      TODO: 把 const 那个 [] 函数注释掉，然后看看下面哪句话会报错
     */
    MyString s1("hello");
    const MyString s2("world");
    cout << s1[0] << endl;
    cout << s2[0] << endl;

    return 0;
}
