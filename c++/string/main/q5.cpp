#include "my_string.h"


using namespace std;


int main() {
    /**
     * 5. 引用作为返回值，除了效率高，还有什么好处？
     *      TODO: 把 operator[] 改成返回值的 & 去掉，然后看下面的函数能否运行
     */
    MyString s1("hello");
    s1[0] = 5;

    return 0;
}
