# Ubuntu

Author : Jiachen Han

<div align=right>
    <img align="right" src="image/ubuntu安装/image-20220907150118241.png" alt="image-20220907150118241" style="zoom:33%;"/>
</div>




[TOC]

## ubuntu是什么

Ubuntu是一个以桌面应用为主的[Linux](https://baike.baidu.com/item/Linux/27050?fromModule=lemma_inlink)操作系统，基于[Debian](https://baike.baidu.com/item/Debian/748667?fromModule=lemma_inlink)发行版和Gnome桌面环境。Linux，全称GNU/Linux，是一个基于[POSIX](https://baike.baidu.com/item/POSIX/3792413?fromModule=lemma_inlink)的多用户、[多任务](https://baike.baidu.com/item/多任务/1011764?fromModule=lemma_inlink)、支持[多线程](https://baike.baidu.com/item/多线程/1190404?fromModule=lemma_inlink)和多[CPU](https://baike.baidu.com/item/CPU/120556?fromModule=lemma_inlink)的操作系统。相比Window，他开源，低功耗。

我们今后的编程环境基本都基于Ubuntu系统。

### 什么是操作系统 OS

1. 计算机是什么

   计算机是一个非常广泛的概念，大到占用数层楼的用于科学计算的超级计算机（天河，神威太湖之光...），小到手机上的嵌入式芯片都可以称为计算机，虽然他们外形不同，但都归于“计算”这一概念。我们暂时将计算机的范围限定在最为流行的，使用最为广泛的PC机（x86-64指令集的个人计算机），其他计算机可能有着不同的架构，但都需要实现类似的功能。

2. 计算机硬件（了解）

   计算机硬件纷繁复杂，其中有不同厂商，不同协议，不同接口。我们抓住其中的三个部件：**CPU，内存，I/O控制芯片及其存储**，对于c/cpp，我们只需要关注这三个部分，就可以掌握计算机的核心，而对于JAVA，.NET等语言我们甚至不需要关注计算机本身，只需要关注平台提供的虚拟机即可。

   ![image-20220907152857136](image/ubuntu安装/image-20220907152857136.png)

   我们一般将计算机系统分为**硬件**和**软件**部分，而软件部分可以分为**系统软件**和应用软件。

   应用应用软件是最上层的软件，例如浏览器，播放器，游戏等。

   应用程序使用操作系统提供的**应用程序编程接口 ( Application Programming Interface)**，编程接口的提供者是**运行库**，例如Linux提供POSX的API，Windows提供Windows API。运行库使用操作系统的提供的**系统调用接口（System call Interface）**，这些接口使用CPU的指令，以软件中断的方式提供。

   |        应用程序         |
   | :---------------------: |
   |  应用程序编程接口 API   |
   |   **运行库**（环境）    |
   |    系统调用接口 SCI     |
   |      **操作系统**       |
   | CPU指令（硬件厂家规定） |
   |         **CPU**         |

   加粗为实体，细体为接口，下层向上层提供，上层负责使用。

3. 为什么我们需要操作系统

   操作系统负责管理硬件资源，提供抽象编程接口，我们进行软件开发离不开操作系统的帮助，操作系统是软件发展过程中，方便进行人机交互，高效使用硬件资源的重要软件。

## 环境配置

### 虚拟机安装

*注：学长能力有限，不能枚举全部错误。我尽量结构化安装步骤，并解释这一步是在干什么，不能避免其中的某些跳步，跳步说明按默认走就ok。*

*如果遇到了什么问题，请学会一个技能（也可能是大学内学会的最有用的技能）：*

*STFW（Search the Fucking Web）*

#### 虚拟机是什么

虚拟机（Virtual Machine）指通过[软件](https://baike.baidu.com/item/软件/12053?fromModule=lemma_inlink)模拟的具有完整[硬件](https://baike.baidu.com/item/硬件?fromModule=lemma_inlink)系统功能的、运行在一个完全[隔离](https://baike.baidu.com/item/隔离/33079?fromModule=lemma_inlink)环境中的完整[计算机系统](https://baike.baidu.com/item/计算机系统/7210959?fromModule=lemma_inlink)。

| 模拟的操作系统1（Linux） \| 模拟的操作系统2（Windows 11）\| mac |
| :----------------------------------------------------------: |
| **虚拟机**运行在**应用程序**层（他是一个应用程序，用软件方式模拟了计算机硬件） |
|                     应用程序编程接口 API                     |
|                      **运行库**（环境）                      |
|                       系统调用接口 SCI                       |
|                         **操作系统**                         |
|                   CPU指令（硬件厂家规定）                    |
|                           **CPU**                            |

#### 前期准备

在这里我选择的是Vmware + ubuntu 18.04进行环境搭建，考虑到有些电脑厂家良心缺失，裸机适配稀烂，这里提供了虚拟机安装教程，推荐自行尝试裸机安装（注意备份原本的Windows系统）。



我们需要下载两个软件：

其中第一个以.exe结尾

![image-20220910155742318](image/ubuntu安装/image-20220910155742318.png)

官网：

https://www.vmware.com/cn/products/workstation-pro/workstation-pro-evaluation.html

VMware下载地址：

https://download3.vmware.com/software/WKST-1624-WIN/VMware-workstation-full-16.2.4-20089737.exe



第二个以.iso结尾

不要纠结他的扩展名。

*该文件包含了ubuntu系统的自己，应用软件的安装取决于操作系统的API，而操作系统本身的安装，就需要取决于硬件厂商，在裸机的安装中，你们也会接触到Bios，并进行操作系统的安装。*

![image-20220910155751026](image/ubuntu安装/image-20220910155751026.png)

ubuntu 资源网站：

![image-20220910160942186](image/ubuntu安装/image-20220910160942186.png)

[Ubuntu 18.04.6 LTS (Bionic Beaver)](https://releases.ubuntu.com/bionic/) 。

ubuntu18.04下载地址:

https://releases.ubuntu.com/bionic/ubuntu-18.04.6-desktop-amd64.iso

------

名词解释：

1. 虚拟机 : 可能指代VMware软件，可能指代里面运行的系统
2. 宿主机：指代运行VMware的系统，也就是你的windows及其硬件
3. 终端：运行指令的界面
4. [TAB]：后续输入命令时的符号，意思是按键盘左侧大写锁定上面的tab， ubuntu能自动帮助你补全后面的内容。
5. sudo后输入的指令：认为有管理员权限，下一行会让你输入你的密码，输入设置的密码即可，注意密码不会回显。*回显指不会像qq一样会给你一堆点告诉你输入了*。

------

#### Vmware 安装

傻瓜式安装到最后结束，输入一个激活码（网上随便找一个），即可直接激活使用。



```
激活密钥
VMware 许可证密钥，批量永久激活
VM16：ZF3R0-FHED2-M80TY-8QYGC-NPKYF
VM15：FC7D0-D1YDL-M8DXZ-CYPZE-P2AY6
VM12：ZC3TK-63GE6-481JY-WWW5T-Z7ATA
VM10：1Z0G9-67285-FZG78-ZL3Q2-234JG
 
 
vmware16最新许可证密钥：
ZF3R0-FHED2-M80TY-8QYGC-NPKYF
 
YF390-0HF8P-M81RQ-2DXQE-M2UT6
 
ZF71R-DMX85-08DQY-8YMNC-PPHV8
 
FA1M0-89YE3-081TQ-AFNX9-NKUC0
 
VMware Workstation 16 永久激活密钥
ZF3R0-FHED2-M80TY-8QYGC-NPKYF
 
YF390-0HF8P-M81RQ-2DXQE-M2UT6
 
ZF71R-DMX85-08DQY-8YMNC-PPHV8
 
 
 
VMware Workstation 15 永久激活密钥
YG5H2-ANZ0H-M8ERY-TXZZZ-YKRV8
 
UG5J2-0ME12-M89WY-NPWXX-WQH88
 
UA5DR-2ZD4H-089FY-6YQ5T-YPRX6
 
GA590-86Y05-4806Y-X4PEE-ZV8E0
 
YA18K-0WY8P-H85DY-L4NZG-X7RAD
 
ZF582-0NW5N-H8D2P-0XZEE-Z22VA
```

安装成功的截图：

![image-20220907163645865](image/ubuntu安装/image-20220907163645865.png)

#### 系统安装

##### 基础安装

1. 准备好下载好的ISO光盘映像文件

   ![image-20220907163730415](image/ubuntu安装/image-20220907163730415.png)

2. 在Vmware中选择新建虚拟机，选择自定义

   <img src="image/ubuntu安装/image-20220907163903531.png" alt="image-20220907163903531" style="zoom: 80%;" />

3. 选择ISO文件

   <img src="image/ubuntu安装/image-20220907164042367.png" alt="image-20220907164042367" style="zoom:80%;" />

4. 自定义设置

   这里设置了在操作系统中的用户名和密码，这些都是由于Vmware已经帮你识别好了该系统，并帮助你简化了安装程序。

   **这里的全名，用户名，密码随意设置，没必要和我相同。**

   <img src="image/ubuntu安装/image-20220907164320349.png" alt="image-20220907164320349" style="zoom:80%;" />

5. 选择安装位置

   <img src="image/ubuntu安装/image-20220907164414970.png" alt="image-20220907164414970" style="zoom:80%;" />

6. 默认CPU核心和内存大小

7. 网络设置

   选择NAT转换。（推荐，主要是这个省事）*系统维护一个ip地址的转换表，工作在网络层，虚拟机帮你转换ip地址，类似于将宿主机当成公网。*

   ![image-20220910163029884](image/ubuntu安装/image-20220910163029884.png)

8. 选择默认I/O控制器类型和磁盘类型

   ![image-20220910163101732](image/ubuntu安装/image-20220910163101732.png)

   ![image-20220910163109264](image/ubuntu安装/image-20220910163109264.png)

9. 选择创建新虚拟磁盘

   <img src="image/ubuntu安装/image-20220907164645942.png" alt="image-20220907164645942" style="zoom:80%;" />

10. 分配磁盘，建议稍微大些，需要安装一些环境，今后课内做实验也简单些。

   <img src="image/ubuntu安装/image-20220907164822572.png" alt="image-20220907164822572" style="zoom:80%;" />

11. 选择一个虚拟磁盘文件位置

    <img src="image/ubuntu安装/image-20220907165004647.png" alt="image-20220907165004647" style="zoom:80%;" />

    至此Vmware操作基本完成

##### Ubuntu安装

上述完成后启动虚拟机，会自动安装Ubuntu系统，一开始黑屏一段时间别慌。



<img src="image/ubuntu安装/image-20220907165153679.png" alt="image-20220907165153679" style="zoom:80%;" />



2. 查看是否有网

   ![image-20220907171517864](image/ubuntu安装/image-20220907171517864.png)



#### 扩展安装

1. 目前虚拟机很丑，没有共享文件夹以及设备驱动适配，我们可以选择安装VMware tools进行增强。

2. （Vmware）关闭虚拟机，进入虚拟机设置

   <img src="image/ubuntu安装/image-20220907172731199.png" alt="image-20220907172731199" style="zoom:80%;" />

   将CD/DVD，软盘全部设置为自动检测。

3. （Vmware）重新启动虚拟机，点击虚拟机，安装VMware tools（这里截图是安装完成之后的，已经安装就会显示这样）

   ![image-20220911161526978](image/ubuntu安装/image-20220911161526978.png)

   Vmware会自动帮助你下载一个压缩包到你的ubuntu中，等待几秒会弹出一个界面。
   ![image-20220911161602274](image/ubuntu安装/image-20220911161602274.png)

   我们接下来在ubuntu中完成增强的安装。

4. （Ubuntu）

   1. 在系统弹出的文件夹中右键打开终端（菜单最后一个Open Terminal）

   2. 此文件夹内有一个以.tar.gz结尾的压缩文件，输入下面命令解压该文件到 /opt文件夹下
       前文有提到[TAB]相关解释，注意空格。再强调一下！！！！！不是直接输入TAB，而是按一下键盘上的TAB自动补全文件名！！！

       ```shell
       sudo tar -zxvf VMwareTools-[TAB] -C /opt/
       ```

       ![image-20220911161649798](image/ubuntu安装/image-20220911161649798.png)

   3. 输入如下命令，进行扩展的安装

       ```shell
          cd /opt
          cd vmware[TAB]
          sudo ./vmware[TAB].pl 
       ```

   4. 第一次停顿的地方输入y回车，之后一路回车。

   5. 安装成功

       ![image-20220911161749068](image/ubuntu安装/image-20220911161749068.png)

5. 可以看到现在屏幕分辨率达到正常（如果不行尝试重启）

   ![image-20220907174846296](image/ubuntu安装/image-20220907174846296.png)

6. （Ubuntu）输入命令实现文件拖拽，共享剪切板等

   ```shell
   sudo apt-get autoremove open-vm-tools
   sudo apt-get update
   sudo apt-get install open-vm-tools-desktop
   最后，使用下面这个命令重启。
   reboot
   ```

7. 测试在宿主机复制内容是否可以在虚拟机内粘贴

8. 大功告成


​    

### 裸机安装（推荐）

[windows10安装ubuntu双系统教程（绝对史上最详细） - 不妨不妨，来日方长 - 博客园 (cnblogs.com)](https://www.cnblogs.com/masbay/p/10745170.html)



