# ubuntu基本使用

## 终端命令入门

linux的使用必然离不开终端命令的使用，接下来没有接触过linux的同学先看下面的内容入个门。

### 打开终端

首先先按 `ctrl+alt+t` 可以打开一个终端，如下图：

![image-20220909211744262](image/ubuntu%E5%9F%BA%E6%9C%AC%E4%BD%BF%E7%94%A8/image-20220909211744262.png)

此时，屏幕里就弹出了一个窗口，这是可以往里面输入命令了。

### 直观理解终端

这里为了方便理解，我们把终端和文件管理器（下图）作一个类比，以实现一个直观的理解。

![image-20220909212750224](image/ubuntu%E5%9F%BA%E6%9C%AC%E4%BD%BF%E7%94%A8/image-20220909212750224.png)

-   终端在工作时，会有一个**工作路径**，类比文件管理器，就是现在的 `用户文件夹`
-   终端**可以切换工作路径**，类比在文件管理器中，双击一个文件夹进入这个文件夹的过程
-   终端可以**在指定路径下做一些操作**，类比在文件管理器中，我可以给这个路径下的某个文件重命名

### 切换工作路径

在打开的终端中，我们可以看到，在光标之前的内容是 `...:~$`，这串字母的意义如下：

-   `...`（上图中为`chengrong@Legion`）：表示我这台电脑这个用户
-   `~`：这部分字母的意义就是终端当前的**工作路径**

#### 第一个命令：ls

可以输入 `ls` 命令来查看当前目录下有哪些文件，如下图：

![image-20220909214744125](image/ubuntu%E5%9F%BA%E6%9C%AC%E4%BD%BF%E7%94%A8/image-20220909214744125.png)

#### cd 命令

`cd` 命令的功能是切换工作路径，具体用法是：

```shell
cd <即将要进入的路径>
```

 比如像下图所示，输入 `cd Downloads`，然后按 `Enter`，我们就执行了 `cd` 命令：

![image-20220909214857319](image/ubuntu%E5%9F%BA%E6%9C%AC%E4%BD%BF%E7%94%A8/image-20220909214857319.png)

此时，我们的工作路径就已经变成了`~/Downloads`

#### 提示

-   linux下命令可以自动补全，比如上面的 `cd Downloads`，只要大家输入 `cd Dow`后，连续按`tab`，就可以自动补全出来
-   linux下还有很多命令，教大家这两个常用的命令只是为了让大家入个门，大家在后续使用的使用可以逐渐地自己去寻找并掌握更多有用的命令

## 在指定路径下做一些操作

这里只教大家如何去运行一个当前工作路径下的一个可以执行的文件。

比如，当前路径下有一个 `hello` 可执行文件，执行它会打印出 `Hello World!` 到屏幕上。

可以输入这个文件的路径，即 `./<文件名>`来运行这个文件（`.`可以代表当前所在的工作路径，`..`可以代表上级目录），如下图：

![image-20220909220140514](image/ubuntu%E5%9F%BA%E6%9C%AC%E4%BD%BF%E7%94%A8/image-20220909220140514.png)

回过头来，我们重新思考 `ls`命令，本质上来说，我们运行 `ls`，依然是通过上面的方式执行的。

只不过，`ls`命令的可执行文件存在于`/bin`下，这里是`ubuntu`的默认可执行文件所在的路径，因此直接输入`ls`系统可以自己找到`ls`可执行文件的位置并执行。