# apt的使用

## 换源

-   目的：由于ubuntu的软件源建立在国外，因此访问起来很慢，所以就需要将软件源换成国内的镜像源（这些镜像源会定期地更新内容与ubuntu的软件源一致）。

ubuntu的软件源配置文件在 `/etc/apt/` 下，可以使用下面的命令在对改文件进行改写：

```shell
sudo gedit /etc/apt/sources.list
```

然后==根据自己的系统==选择下面的源，写在 `sources.list`里

![image-20220911174800151](image/apt%E7%9A%84%E4%BD%BF%E7%94%A8/image-20220911174800151.png)

-   `ubuntu18`

    -   ```
        deb http://mirrors.aliyun.com/ubuntu/ bionic main restricted universe multiverse
        deb http://mirrors.aliyun.com/ubuntu/ bionic-security main restricted universe multiverse
        deb http://mirrors.aliyun.com/ubuntu/ bionic-updates main restricted universe multiverse
        deb http://mirrors.aliyun.com/ubuntu/ bionic-proposed main restricted universe multiverse
        deb http://mirrors.aliyun.com/ubuntu/ bionic-backports main restricted universe multiverse
        deb-src http://mirrors.aliyun.com/ubuntu/ bionic main restricted universe multiverse
        deb-src http://mirrors.aliyun.com/ubuntu/ bionic-security main restricted universe multiverse
        deb-src http://mirrors.aliyun.com/ubuntu/ bionic-updates main restricted universe multiverse
        deb-src http://mirrors.aliyun.com/ubuntu/ bionic-proposed main restricted universe multiverse
        deb-src http://mirrors.aliyun.com/ubuntu/ bionic-backports main restricted universe multiverse
        ```

-   `ubuntu20`

    -   ```
        deb http://mirrors.aliyun.com/ubuntu/ focal main restricted universe multiverse
        deb-src http://mirrors.aliyun.com/ubuntu/ focal main restricted universe multiverse
        deb http://mirrors.aliyun.com/ubuntu/ focal-security main restricted universe multiverse
        deb-src http://mirrors.aliyun.com/ubuntu/ focal-security main restricted universe multiverse
        deb http://mirrors.aliyun.com/ubuntu/ focal-updates main restricted universe multiverse
        deb-src http://mirrors.aliyun.com/ubuntu/ focal-updates main restricted universe multiverse
        deb http://mirrors.aliyun.com/ubuntu/ focal-proposed main restricted universe multiverse
        deb-src http://mirrors.aliyun.com/ubuntu/ focal-proposed main restricted universe multiverse
        deb http://mirrors.aliyun.com/ubuntu/ focal-backports main restricted universe multiverse
        deb-src http://mirrors.aliyun.com/ubuntu/ focal-backports main restricted universe multiverse
        ```

-   `ubuntu22`

    -   ```
        deb http://mirrors.aliyun.com/ubuntu/ jammy main restricted universe multiverse
        deb-src http://mirrors.aliyun.com/ubuntu/ jammy main restricted universe multiverse
        deb http://mirrors.aliyun.com/ubuntu/ jammy-security main restricted universe multiverse
        deb-src http://mirrors.aliyun.com/ubuntu/ jammy-security main restricted universe multiverse
        deb http://mirrors.aliyun.com/ubuntu/ jammy-updates main restricted universe multiverse
        deb-src http://mirrors.aliyun.com/ubuntu/ jammy-updates main restricted universe multiverse
        deb http://mirrors.aliyun.com/ubuntu/ jammy-proposed main restricted universe multiverse
        deb-src http://mirrors.aliyun.com/ubuntu/ jammy-proposed main restricted universe multiverse
        deb http://mirrors.aliyun.com/ubuntu/ jammy-backports main restricted universe multiverse
        deb-src http://mirrors.aliyun.com/ubuntu/ jammy-backports main restricted universe multiverse
        ```

然后 `ctrl + s` 保存，关闭。

## 基本使用

 `ctrl+alt+t`，打开一个终端

* 软件源更新
  * 这个命令的功能是：类比手机软件商店，每次打开都告诉你哪些软件需要
  
  * ```shell
    sudo apt update
    ```
    
  * ![image-20220911174936736](image/apt%E7%9A%84%E4%BD%BF%E7%94%A8/image-20220911174936736.png)
  
* 软件更新
  * 在软件源为最新的情况下，就可以使用下面的命令来实现软件的更新
  * ```shell
    sudo apt upgrade
    ```
    
  * ![image-20220911175001841](image/apt%E7%9A%84%E4%BD%BF%E7%94%A8/image-20220911175001841.png)
  
* 从软件源下载软件
  * 从软件源下载软件，依然建议先更新软件源信息，然后执行以下命令来下载软件
  
  * ```shell
    sudo apt install <软件名>
    ```
  
* 安装本地deb包软件
  * ```shell
    sudo apt install ./<deb包名>
    ```
  * `./` 不可缺少，意义是告诉 apt 这里下载的软件不是软件源里的软件，而是当前目录下的软件
  * PS：`<deb包名>` 往往与 `<软件名>` 不同，在执行完上面的命令后，往往 apt 会输出一些信息，其中就包含了软件的真实名字，可以在之后的卸载软件时使用
  
* 卸载软件
  * ```shell
    sudo apt purge <软件名>
    ```

## 使用习惯

建议大家定期执行一下软件源更新和软件更新的命令，否则时间久了更新一次会要很长时间，而且容易出依赖问题。

