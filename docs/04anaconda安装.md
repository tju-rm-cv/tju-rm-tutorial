按照教程来就行：https://zhuanlan.zhihu.com/p/426655323

ubuntu终端中conda的使用：

1. `conda install xxx`，用于安装python第三方库，xxx是库名。
2. `conda list`，查看现在已有的第三方库。
3. `conda create --name xxx`，创建新的虚拟环境，xxx是环境名
   什么叫虚拟环境？
   虚拟环境可以当做是第三方库的集合，由于不同的代码对第三方库的版本要求不同，所以需要在不同的虚拟环境下执行，这也是anaconda的优势所在。默认环境是`base`环境。
4. `conda activate xxx`，切换到某个环境，xxx是环境名。

