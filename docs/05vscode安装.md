# vscode的安装

vscode是一个非常好用的编辑器，美观并且有很多强大的插件，强烈推荐大家使用。

### 安装

直接打开终端复制下面的命令

```bash
wget https://vscode.cdn.azure.cn/stable/784b0177c56c607789f9638da7b6bf3230d47a8c/code_1.71.0-1662018389_amd64.deb

sudo apt install ./code_1.71.0-1662018389_amd64.deb
```

### 必备插件

-   如何安装插件：点击这个玩意，然后在上面搜索，找到后安装即可。
    -   ![1662550070827](image/vscode%E5%AE%89%E8%A3%85/1662550070827.png)
-   插件1：chinese (language)：vscode汉化
    -   ![image-20220911170723551](image/vscode%E5%AE%89%E8%A3%85/image-20220911170723551.png)
    -   点击 `install` 安装，然后重启 vscode
-   插件2：C/C++
    -   ![image-20220911170814572](image/vscode%E5%AE%89%E8%A3%85/image-20220911170814572.png)
-   插件3：python
    -   ![image-20220911170827196](image/vscode%E5%AE%89%E8%A3%85/image-20220911170827196.png)

### 更多插件推荐(可选)

下面是我使用的一些插件，已经给大家分好类了，大家自己选择使用。

- 通用
  - chinese (language)：vscode汉化
  - Office Viewer：office文档预览，支持docx、pptx、xlsx、md等
  - Material Icon Theme：vscode 图标主题（有的没的插件）
- 开发通用
  - Project Manager：项目管理
  - Comment Translate：注释翻译
  - Error Lens：把错误检查直接显示在代码对应行上
  - Path Intellisense：路径自动补全
  - Rainbow Brackets：彩虹括号
  - Trailing Spaces：高亮显示多余的空格
  - Intellicode：智能提示，自动补全有顺序优化，重构代码时也有用
  - GitHub Copilot：代码自动补全，特别好用，但是已经开始收费了，学生可以在 [https://education.github.com/globalcampus/student](https://education.github.com/globalcampus/student) 申请免费
- ssh
  - Remote - SSH：远程ssh
  - Remote - SSH: Editing Configuration Files：远程ssh配置文件，配置文件一般都在 `~/.ssh/config` 下
- C/C++
  - C/C++：C/C++语言自动补全、语法检查支持
  - CMake：CMake语法高亮和自动补全
- python
  - Python：python语言自动补全、语法检查支持
  - Pylance：python语言自动补全、语法检查支持
- java: 没怎么用过 java，只知道用下面的就能开发 java 了
  - Language Support for Java(TM) by Red Hat：java语法高亮和自动补全
  - Debugger for Java：Java调试
  - Extension Pack for Java：Java开发必备插件
  - Maven for Java：Maven语法高亮和自动补全
  - Project Manager for Java：Java项目管理
  - Test Runner for Java：Java测试
- 汇编：也没怎么用过
  - MASM/TASM：汇编语言高亮和自动补全，可以给出指令的官网文档链接
- git
  - Git History：git历史记录
  - GitLens：git信息显示（可以具体显示到每一行代码是哪个**人写的）
- latex
  - LaTeX Workshop：LaTeX语法高亮和自动补全
