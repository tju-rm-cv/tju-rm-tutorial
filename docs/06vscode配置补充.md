###### 界面配置

- **vscode汉化**

  1. 点击左侧这个图标，进入扩展（Extention）
     ![image-20220911203845509](image/vscode%E9%85%8D%E7%BD%AE%E8%A1%A5%E5%85%85/image-20220911203845509.png)
  2. 搜索`Chinese`，在右侧界面中点击`Install`安装，重启之后就是中文了。

- **vscode界面美化**

  1. `'ctrl' + '+'`可以扩大界面的字体，`'ctrl' + '-'`可以缩小界面字体，加减号就是键盘右上角那里的加减号，跟数字在同一行。

  2. 点击左下角的小齿轮，可以进入设置界面。

  3. 修改编辑器字体大小：
     <img src="image/vscode%E9%85%8D%E7%BD%AE%E8%A1%A5%E5%85%85/image-20220911204013259.png" alt="image-20220911204013259" style="zoom:50%;" />

  4. 修改编辑器的字体：
     <img src="image/vscode配置补充/49946896/2022-09-09 15-19-38 的屏幕截图.png" style="zoom:50%;" />

     默认有一个字体叫`monospace`，等宽字体，还不错。
     推荐字体：`Fira Code`，`Consolas`，安装方法自行百度。

  5. 修改tab的大小为4个空格（python严格要求）
     <img src="image/vscode配置补充/91ada012/2022-09-09 15-25-31 的屏幕截图.png" style="zoom: 50%;" />

  6. 打开文件自动保存，这样就不用时时刻刻按ctrl + S保存了。
     <img src="image/vscode配置补充/8cdf3065/2022-09-09 15-26-38 的屏幕截图.png" style="zoom: 50%;" />

  7. 扩展里面安装`One Dark Pro`主题，这种黑色界面适合编程。用白色界面的人，我只能说注意保护眼睛（审美也有待进步）。
     <img src="image/vscode配置补充/1ef40ba7/2022-09-09 15-23-14 的屏幕截图.png" style="zoom: 33%;" />

- **设置一下终端的字体**

  1. 打开设置，找到功能 -> 终端。
  3. 设置终端的字体与大小（不然很丑）。
     <img src="image/vscode配置补充/f0ca15c3/2022-09-09 15-30-58 的屏幕截图.png" style="zoom:50%;" />
  
- **安装c/c++插件，实现自动补全，纠错功能（一般会主动提示你安装）**
  <img src="image/vscode配置补充/8f103d95/2022-09-09 15-38-26 的屏幕截图.png" style="zoom:50%;" />

- **开启Code Runner，一键运行c/c++/python代码**

  1. 打开扩展，安装`Code Runner`。

  2. 进入设置 -> 扩展 -> Run Code Configuration，开始对Code Runner的配置。

  3. 开启以下3个选项。
     <img src="image/06vscode配置补充/2022-09-11 21-42-53 的屏幕截图-1662903896961-2.png" style="zoom:33%;" />
     第一个选项表示代码在终端中执行，后两个选项与代码自动保存有关。
  
  4. 编写一个测试用的文件，这边以c++语言为例。<img src="image/vscode配置补充/7fe40d/2022-09-09 15-39-34 的屏幕截图.png" style="zoom:50%;" />

  5. 右键点击代码编辑器里任意空白位置，选择`Run Code`。
     <img src="image/vscode配置补充/c7a85a7a/2022-09-09 15-40-59 的屏幕截图.png" style="zoom: 25%;" />
  
  6. 可以看到代码执行结果会出现在内嵌终端中，如下图所示：
     ![](image/vscode配置补充/93eb2685/2022-09-09 15-41-59 的屏幕截图.png)
  
     这样就能一键运行代码啦。
     最后再说一下Code Runner是怎么做到一键运行代码的，毕竟，对于一件事情我们最好要知其所以然对吧。
     可以看到在程序输出结果的上面，有一行这个东西：

     ```shell
     cd "/home/nobug/Documents/oblivion/vscode-test/" && g++ test.cc -o test && "/home/nobug/Documents/oblivion/vscode-test/"test
     ```
  
     实际上呢，这是一个由`&&`连接起来的，连续执行的终端指令序列，可以分成3步：

     ```shell
     (1) cd "/home/nobug/Documents/oblivion/vscode-test/" // 进入当前代码所在的文件夹
     (2) g++ test.cc -o test // 调用编译器进行编译，编译出的可执行文件叫test
     (3) "/home/nobug/Documents/oblivion/vscode-test/"test // 运行test
     ```
  
     也就是说，它的原理实际上很简单，就是自动帮你执行一些简单的代码编译指令。把语言换成python，也是一样的。
     <img src="image/vscode配置补充/a1a30f17/2022-09-09 15-48-08 的屏幕截图.png" style="zoom: 50%;" />
  
     也是调用了python解释器来执行python代码。

如果你觉得这样的界面好看的话，那咱们审美差不多哈哈。
