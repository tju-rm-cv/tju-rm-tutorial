# opencv的安装

opencv是一个图像处理包。

## 安装

对于 python 版本的 opencv，安装只需要使用 pip 执行以下命令：

```shell
pip install opencv-python
```

对于 c++ 版本的 opencv，直接使用 apt 进行安装：

```shell
sudo apt install libopencv-dev
```

## 检查c++版本是否安装成功

1. **下载测试程序**
	- [从此处下载测试程序zip](assets/opencv-test.zip)
2. **解压文件夹**
    -   ![image-20220911200839612](image/opencv%E5%AE%89%E8%A3%85/image-20220911200839612.png)
3. **进行测试**
	1. 编译
		- ![image-20220911200938478](image/opencv%E5%AE%89%E8%A3%85/image-20220911200938478.png)
		- 如果 `cmake ..` 之后，没有出现红色的报错信息，而是打印像上面的信息一样，**说明 `opencv` 已经安装成功**
	2. 运行
		- ![image-20220911201305269](image/opencv%E5%AE%89%E8%A3%85/image-20220911201305269.png)
	3. 代码运行效果如下
		- ![image-20220911201332254](image/opencv%E5%AE%89%E8%A3%85/image-20220911201332254.png)
