由于不同的设备和代码, 需要安装的版本不同

所以, 本教程主要结合 nvidia 官方文档, 讲解如何安装 tensorRT

以便大家能自由地安装自己需要的版本

所以阅读时注意: 一定要**亲自打开官方文档**, 去尝试着看

# 1 几个概念

## 1.1 显卡驱动

装了之后, 系统能够使用nvidia显卡了, 安装成功有以下两个特征

- 设置的关于里, 图形那一栏变成了nvidia
  - ![1677470669483](assets/09tensorRT安装/1677470669483.png)
- nvidia-smi命令能够正常使用
  - ![1677470873293](assets/09tensorRT安装/1677470873293.png)

## 1.2 nvidia-smi

nvidia-smi是nvidia提供的一个命令行工具, 可以用来查看显卡的信息, 例如显存占用, 温度, 显卡型号等等

有必要解释一下, 命令输出右上角的 cuda 字样, **是指显卡最高支持的cuda版本, 而不是cuda的版本**

例如我的显卡支持cuda-toolkit 12.0, 但是我可以安装任何 <= 12.0 版本的 cuda-toolkit, 如下图(我同时安装了12.0 和 11.8 两种版本)

![1677471008896](assets/09tensorRT安装/1677471008896.png)

PS: `ubuntu` 中的 `alternatives` 命令可以用来管理多个版本的软件, 包括 `gcc` 等等, 可以让你方便地切换不同版本 (本质上是软链接, 可以查看 `linux` 的 `ln` 命令了解软链接)

## 1.3 cuda-toolkit

cuda-toolkit 是nvidia提供的一个开发工具包, 包含了cuda的编译器, 运行时, 以及一些工具, 例如 `nvcc` 等等

## 1.4 cudnn

cudnn 是nvidia专门为深度神经网络设计的一个加速库

## 1.5 pytorch 和上面东西的关系

如果你仅仅是 `pytorch` 需要使用 `gpu`, **那你不需要安装 `cuda-toolkit` 和 `cudnn`**, 因为 `pytorch` 从很早的版本开始就有人家有自己的, 如下图

![1677471429939](assets/09tensorRT安装/1677471429939.png)

图中说明了我的 `pytorch` 自带了编译好的 `cuda 11.7`, 而我上面刚讲过我的nvidia驱动最高支持 `cuda 12.0`.

## 1.5 tensorRT

`tensorRT` 是为了部署神经网络, 在我们 RM 中要将 `pytorch` 模型用 `c++` + `tensorRT` 部署.

对于这种情况, 必须安装 `cuda` 和 `cudnn`, 因为 `tensorRT` 是基于 `cuda` 和 `cudnn` 的(下文有说明).

# 2 安装版本选择

首先我们应该看 `tensorRT` 应该选择什么版本.

1. 搜索引擎搜索tensorRT, 找到官网(应该要注册)
2. 首先看计算设备上支持的 `cuda` 版本, 然后根据 [tensorRT官网](https://developer.nvidia.cn/nvidia-tensorrt-8x-download) 一个一个点开看它需要的 `cuda` 版本, 直接排除不支持的版本
   - ![1677471910747](assets/09tensorRT安装/1677471910747.png)
3. 然后, 如果你当前写了代码, 要大概了解一下代码能在哪些版本里跑通
    - 例如我们2022年比赛代码中这一部分:
    - ![1677472065891](assets/09tensorRT安装/1677472065891.png)
    - 所以最新版本不一定就是最适合你的

# 3 安装过程

**看官方文档!!!**

## 3.1 cuda和cudnn版本选择

1. 找到刚刚你**已经确定好的版本**, 比如我要安装 `tensorRT 8.5 GA Update 2`, 点击它
2. 查看官方文档
    - ![1677472458641](assets/09tensorRT安装/1677472458641.png)
3. 找到对应版本, 看到有一个 `Installation Guide`
    - ![1677472561553](assets/09tensorRT安装/1677472561553.png)
4. 打开
    - ![1677472674330](assets/09tensorRT安装/1677472674330.png)
5. 这时就知道了, **cuda** 安装这里的随便一个版本(这里我选择 11.8), **cudnn** 安装 8.6.0
6. 点击官方文档里这两个超链接打开 `cuda` 和 `cudnn` 下载官网

## 3.2 下载加速

后续所有的下载中(一共三个 `deb` 包), 我们在下载时, 都要将**下载链接**中的 `.com` 改成 `.cn`, 这样就可以使用国内的下载源, 加速下载

## 3.3 cuda安装

1. 回到刚刚打开的 `cuda` 官网, 选择 11.8 版本
2. 根据实际情况填表
3. 执行下面的命令 (记得 `wget` 时 改成 `.cn`)

![1677476037264](assets/09tensorRT安装/1677476037264.png)

PS: 如果你电脑上原本安装过 `cuda`, 即现在你有两个 `cuda` 版本, 可以使用 `update-alternatives` 命令来切换版本 (可以看前文的图片)

## 3.4 上述命令解释

1. 将 `apt` 默认源中的 `cuda` 软件源优先级设置为最低, 防止后面安装时使用默认的源
   - 默认源优点是安装方便
   - 缺点是
     - **版本难以随意选择**
     - 下载可能还慢

    ```shell
    wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2204/x86_64/cuda-ubuntu2204.pin
    sudo mv cuda-ubuntu2204.pin /etc/apt/preferences.d/cuda-repository-pin-600
    ```
2. 下载该版本 cuda 的所有 deb 包以及密钥
    ```shell
    wget https://developer.download.nvidia.com/compute/cuda/11.8.0/local_installers/cuda-repo-ubuntu2204-11-8-local_11.8.0-520.61.05-1_amd64.deb
    ```
3. 安装刚刚的 deb 包以及密钥
   - 这里官方文档用的 `dpkg`, 我们其实用 `apt` 更好(注释部分)
    ```shell
    sudo dpkg -i cuda-repo-ubuntu2204-11-8-local_11.8.0-520.61.05-1_amd64.deb
    # sudo apt install ./cuda-repo-ubuntu2204-11-8-local_11.8.0-520.61.05-1_amd64.deb

    # 然后终端也会输出信息, 提示你输入下面的命令
    sudo cp /var/cuda-repo-ubuntu2204-11-8-local/cuda-*-keyring.gpg /usr/share/keyrings/
    ```
4. 安装 cuda
    ```shell
    sudo apt-get update
    sudo apt-get -y install cuda
    ```

其中, 第2步下载的 `deb` 包中, 包含了所有的东西, 包括后续第4步安装的 `cuda` 的 `deb` 包.

我们可以通过以下方式来大致了解 `deb` 包:

1. 双击打开 `deb` 包
    - ![1677476881370](assets/09tensorRT安装/1677476881370.png)
2. 打开 `data.tar.xz`
3. 打开 `.` 文件夹
4. 打开 `var/cuda-repo-ubuntu2204-11-8-local/`
5. 此时可以看到 `cuda` 的所有东西
   - ![1677476994757](assets/09tensorRT安装/1677476994757.png)
   - 如果继续看这些 `deb` 包的内容
     - `control.tar.xz` 里有 `deb` 包之间的依赖关系
     - `data.tar.xz` 里的文件路径就是安装后的路径

## 3.5 cudnn 安装

官方安装文档: [https://docs.nvidia.com/deeplearning/cudnn/install-guide/index.html](https://docs.nvidia.com/deeplearning/cudnn/install-guide/index.html)

### 下载

1. 打开 [`cudnn` 官网](https://developer.nvidia.com/rdp/cudnn-archive), 选择 8.6.0 版本
    - ![1677477577552](assets/09tensorRT安装/1677477577552.png)
2. 点击后, 会开始下载一个 `deb` 包, 但是很慢
3. 然后我们用浏览器, 复制下载链接, 将 `.com` 改成 `.cn`, 放在浏览器地址栏, 按回车开始下载

### 安装

安装方式和 `cuda` 很像, 按照官方文档来就行

![1677478228129](assets/09tensorRT安装/1677478228129.png)

1. 安装依赖
    ```shell
    sudo apt install zlib1g
    ```
2. 安装 `cudnn`
    ```shell
    sudo apt install ./cudnn-local-repo......deb
    sudo cp /var/cudnn-local-repo-*/cudnn-local-*-keyring.gpg /usr/share/keyrings/
    sudo apt update
    sudo apt install libcudnn8
    sudo apt install libcudnn8-dev
    sudo apt install libcudnn8-samples
    ```
    ![1677478006498](assets/09tensorRT安装/1677478006498.png)

## 3.6 tensorRT 安装

1. 回到最初的 `tensorRT`, 点击链接下载
    - ![1677478283665](assets/09tensorRT安装/1677478283665.png)
2. 仍然用 `.cn` 加速
3. 安装(与 `cuda` 和 `cudnn` 类似, 看着官方文档做)
    - ![1677478372866](assets/09tensorRT安装/1677478372866.png)
