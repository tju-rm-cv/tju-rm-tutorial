###### 界面配置

- **vscode汉化**

  1. 点击左侧这个图标，进入扩展（Extention）
     ![](/run/user/1000/doc/786914e6/2022-09-09 15-14-20 的屏幕截图.png)
  2. 搜索`Chinese`，在右侧界面中点击`Install`安装，重启之后就是中文了

- **vscode界面美化（符合我的审美）**

  1. `'ctrl' + '+'`可以扩大界面的字体，`'ctrl' + '-'`可以缩小界面字体，加减号就是键盘右上角那里的加减号，跟数字在同一行。

  2. 点击左下角的小齿轮，可以进入设置界面。

  3. 修改编辑器字体大小：
     <img src="/run/user/1000/doc/fb8a1971/2022-09-09 15-18-34 的屏幕截图.png" style="zoom: 50%;" />

  4. 修改编辑器的字体：
     <img src="/run/user/1000/doc/49946896/2022-09-09 15-19-38 的屏幕截图.png" style="zoom:50%;" />

     默认有一个字体叫`monospace`，等宽字体，还不错。
     推荐字体：`Fira Code`，`Consolas`，安装方法自行百度。

  5. 修改tab的大小为4个空格（python严格要求）
     <img src="/run/user/1000/doc/91ada012/2022-09-09 15-25-31 的屏幕截图.png" style="zoom: 50%;" />

  6. 打开文件自动保存，这样就不用时时刻刻按ctrl + S保存了。
     <img src="/run/user/1000/doc/8cdf3065/2022-09-09 15-26-38 的屏幕截图.png" style="zoom: 50%;" />

  7. 扩展里面安装`One Dark Pro`主题，这种黑色界面适合编程。用白色界面的人，我只能说注意保护眼睛（审美也有待进步）。
     <img src="/run/user/1000/doc/1ef40ba7/2022-09-09 15-23-14 的屏幕截图.png" style="zoom: 33%;" />

- **开启内嵌终端（Ubuntu下这个特别好用）**

  1. 打开设置，找到功能 -> 终端。
  2. 开启内嵌终端。
     <img src="/run/user/1000/doc/92686846/2022-09-09 15-30-13 的屏幕截图.png" style="zoom:50%;" />
  3. 设置终端的字体与大小（不然很丑）。
     <img src="/run/user/1000/doc/f0ca15c3/2022-09-09 15-30-58 的屏幕截图.png" style="zoom:50%;" />
  4. 主界面上侧菜单栏 -> 终端 -> 新建终端，就能够看到内嵌终端了。
     ![](/run/user/1000/doc/d6bbc336/2022-09-09 15-33-47 的屏幕截图.png)

- **安装c/c++插件，实现自动补全，纠错功能（一般会主动提示你安装）**
  <img src="/run/user/1000/doc/8f103d95/2022-09-09 15-38-26 的屏幕截图.png" style="zoom:50%;" />

- **开启Code Runner，一键运行c/c++/python代码**

  1. 打开扩展，安装`Code Runner`。

  2. 编写一个测试用的文件，这边以c++语言为例。<img src="/run/user/1000/doc/7fe40d/2022-09-09 15-39-34 的屏幕截图.png" style="zoom:50%;" />

  3. 右键点击代码编辑器里任意空白位置，选择`Run Code`。
     <img src="/run/user/1000/doc/c7a85a7a/2022-09-09 15-40-59 的屏幕截图.png" style="zoom: 25%;" />

  4. 可以看到代码执行结果会出现在内嵌终端中，如下图所示：
     ![](/run/user/1000/doc/93eb2685/2022-09-09 15-41-59 的屏幕截图.png)

     这样就能一键运行代码啦。
     最后再说一下Code Runner是怎么做到一键运行代码的，毕竟，对于一件事情我们最好要知其所以然对吧。
     可以看到在程序输出结果的上面，有一行这个东西：

     ```shell
     cd "/home/nobug/Documents/oblivion/vscode-test/" && g++ test.cc -o test && "/home/nobug/Documents/oblivion/vscode-test/"test
     ```

     实际上呢，这是一个由`&&`连接起来的，连续执行的终端指令序列，可以分成3步：

     ```shell
     (1) cd "/home/nobug/Documents/oblivion/vscode-test/" // 进入当前代码所在的文件夹
     (2) g++ test.cc -o test // 调用编译器进行编译，编译出的可执行文件叫test
     (3) "/home/nobug/Documents/oblivion/vscode-test/"test // 运行test
     ```

     也就是说，它的原理实际上很简单，就是自动帮你执行一些简单的代码编译指令。把语言换成python，也是一样的。
     <img src="/run/user/1000/doc/a1a30f17/2022-09-09 15-48-08 的屏幕截图.png" style="zoom: 50%;" />

     也是调用了python解释器来执行python代码。

如果你觉得这样的界面好看的话，那咱们审美差不多哈哈。